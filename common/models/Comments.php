<?php

namespace common\models;

use common\models\query\ArticleQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 *
 * @property int $id [int]
 * @property string $user_id [int unsigned]
 * @property string $article_id [int unsigned]
 * @property string $parent_id [int unsigned]
 * @property string $body
 * @property string $created_at [datetime]
 * @property string $updated_at [datetime]
 * @property int $deleted [smallint]
 */
class Comments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comments}}';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'body', 'article_id'], 'required'],
            [['user_id', 'article_id', 'parent_id'], 'integer'],
            [['body'], 'string'],
            [['user_id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            [['article_id'], 'exist', 'targetClass' => Article::class, 'targetAttribute' => 'id'],
            [['parent_id'], 'exist', 'targetClass' => static::class, 'targetAttribute' => 'id'],
            [['deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'body' => Yii::t('common', 'Body'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id']);
    }

    public function delete()
    {
        $this->deleted = true;

        return $this->save();
    }

    public function getNestedComments()
    {
        return $this->hasMany(static::class, ['parent_id' => 'id']);
    }

    /**
     * @param int $article_id
     * @return int
     */
    public static function getArticleCommentsTotalCount(int $article_id):int
    {
        return static::find()->where(['article_id' => $article_id, 'deleted' => false])->count();
    }

    /**
     * @param int $article_id
     * @return array
     */
    public static function getArticleBaseComments(int $article_id):array
    {
        return static::find()
            ->where(['article_id' => $article_id, 'parent_id' => null])
            ->all();
    }

    public function canBeDeleted(): bool
    {
        $comment_datetime = new \DateTime($this->created_at);
        $current_datetime = new \DateTime();
        $diff = $comment_datetime->diff($current_datetime);

        return Yii::$app->user->id === $this->user_id && ($diff->h + $diff->days * 24 < 1);
    }

    public static function createNewCommentObject(int $article_id, int $user_id, int $parent_id = null)
    {
        $comment = new static();
        $comment->article_id = $article_id;
        $comment->user_id = $user_id;
        $comment->parent_id = $parent_id;

        return $comment;
    }
}
