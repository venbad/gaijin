<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comments}}`.
 */
class m221009_090641_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'article_id' => $this->integer()->unsigned()->notNull(),
            'parent_id' => $this->integer()->unsigned(),
            'body' => $this->text(),
            'created_at' => "datetime not null default now()",
            'updated_at' => "datetime not null default now() on update now()",
            'deleted' => $this->smallInteger()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comments}}');
    }
}
