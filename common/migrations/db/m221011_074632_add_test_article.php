<?php

use yii\db\Migration;

/**
 * Class m221011_074632_add_test_article
 */
class m221011_074632_add_test_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('article', [
            'slug' => 'test_article',
            'title' => 'test article',
            'body' => '<p>test article</p>',
            'category_id' => 1,
            'status' => 1,
            'created_by' => 1,
            'updated_by' => 1,
            'published_at' => 1665306126
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221011_074632_add_test_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221011_074632_add_test_article cannot be reverted.\n";

        return false;
    }
    */
}
