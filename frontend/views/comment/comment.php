<?php
/**
 * @var yii\web\View $this
 * @var \common\models\Comments $comment
 * @var integer $comment_level
 * @var array $expanded_comments_ids
 * @var integer $edited_comment_id
 * @var integer $answer_comment_id
 */

use common\models\Comments;
use yii\helpers\HtmlPurifier;

?>
<div class="list-group-item flex-column justify-content-between lh-condensed">
<?php if ($comment->id === $edited_comment_id): ?>
    <?php echo $this->render('comment_form', ['comment_model' => $comment, 'expanded_comments_ids' => $expanded_comments_ids]) ?>
<?php else: ?>
    <?php if ($comment->deleted): ?>
        <p class="text-muted text-sm">
            Comment deleted
        </p>
    <?php else: ?>
        <?php echo $comment_level+1 ?>) <?php echo HtmlPurifier::process($comment->body) ?>
        <p class="text-muted text-sm">
            <?php echo HtmlPurifier::process($comment->author->username) ?>
            <?php echo HtmlPurifier::process($comment->created_at) ?>
        </p>
        <?php if ($comment->canBeDeleted()): ?>
            <?php echo \yii\helpers\Html::a(
                'edit',
                ['comment/edit', 'id' => $comment->id, 'article_id' => $comment->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)])
            ?>
            <?php echo \yii\helpers\Html::a(
                'delete',
                ['comment/delete', 'id' => $comment->id, 'article_id' => $comment->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)])
            ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->id): ?>
            <?php if ($answer_comment_id == $comment->id): ?>
                <?php echo $this->render('comment_form', [
                    'comment_model' => Comments::createNewCommentObject($comment->article_id, \Yii::$app->user->id, $comment->id),
                    'expanded_comments_ids' => $expanded_comments_ids
                ]) ?>
            <?php else: ?>
                <?php echo \yii\helpers\Html::a(
                    'answer',
                    ['comment/answer', 'id' => $comment->id, 'article_id' => $comment->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)])
                ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
<?php if (!empty($comment->nestedComments)): ?>
    <?php if ($comment_level < 2 || in_array($comment->id, $expanded_comments_ids)): ?>
    </div>
        <ul id="article-comments">
            <?php foreach ($comment->nestedComments as $nested_comment): ?>
                <li style="list-style-type: none;">
                    <?php echo $this->render('comment', [
                        'comment' => $nested_comment,
                        'comment_level' => $comment_level+1,
                        'expanded_comments_ids' => in_array($comment->id, $expanded_comments_ids) ? array_merge($expanded_comments_ids, [$nested_comment->id]) : $expanded_comments_ids,
                        'edited_comment_id' => $edited_comment_id,
                        'answer_comment_id' => $answer_comment_id
                    ]) ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <?php echo \yii\helpers\Html::a(
            'expand',
            ['comment/expand', 'id' => $comment->id, 'article_id' => $comment->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)],
            ['class' => 'expand_link', 'data-id' => $comment->id]
        )
        ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
