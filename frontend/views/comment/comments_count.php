<?php
/**
 * @var yii\web\View $this
 * @var integer $comments_count
 */

use yii\widgets\Pjax;

Pjax::begin([
    'id' => 'pjax-comments-count',
    'enablePushState' => false
]); ?>
    <p class="lead"><?php echo Yii::t('frontend', 'Comments') ?>: <?php echo $comments_count ?></p>
<?php Pjax::end(); ?>