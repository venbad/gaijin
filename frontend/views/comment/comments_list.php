<?php
/**
 * @var yii\web\View $this
 * @var array $comments
 * @var array $expanded_comments_ids
 * @var array $edited_comment_id
 * @var array $answer_comment_id
 *
 */

use yii\widgets\Pjax;

Pjax::begin([
    'id' => 'pjax-comments-list',
    'enablePushState' => false
]); ?>
<?php if (!empty($comments)): ?>
    <ul id="article-comments">
        <?php foreach ($comments as $comment): ?>
            <li style="list-style-type: none;">
                <?php echo $this->render('comment', [
                    'comment' => $comment,
                    'comment_level' => 0,
                    'expanded_comments_ids' => $expanded_comments_ids,
                    'edited_comment_id' => $edited_comment_id,
                    'answer_comment_id' => $answer_comment_id
                ]) ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php Pjax::end(); ?>