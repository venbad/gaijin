<?php
/**
 * @var yii\web\View $this
 * @var frontend\models\ContactForm $comment_model
 * @var array $expanded_comments_ids
 */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

?>

<?php
if ($comment_model->id) {
    $action = ['comment/save', 'id' => $comment_model->id, 'article_id' => $comment_model->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)];
} elseif ($comment_model->parent_id) {
    $action = ['comment/reply', 'article_id' => $comment_model->article_id, 'expanded_ids' => implode(',', $expanded_comments_ids)];
} else {
    $action = ['comment/add', 'expanded_ids' => implode(',', $expanded_comments_ids)];
}
$form = ActiveForm::begin([
    'id' => 'comment-form',
    'action' => $action,
    'options' => ['data-pjax' => true]
]); ?>
    <?php echo $form->field($comment_model, 'article_id')->hiddenInput()->label(false) ?>
    <?php echo $form->field($comment_model, 'user_id')->hiddenInput()->label(false) ?>
    <?php echo $form->field($comment_model, 'parent_id')->hiddenInput()->label(false) ?>
    <?php echo $form->field($comment_model, 'body')->textArea(['rows' => 6]) ?>
    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
    </div>
<?php ActiveForm::end(); ?>

