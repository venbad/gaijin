<?php

namespace frontend\controllers;

use common\models\Comments;
use yii\web\Controller;

class CommentController extends Controller
{
    public function actionAdd($expanded_ids)
    {
        $model = new Comments();
        if ($model->load(\Yii::$app->request->post())) {
            $model->save();
        }
        $new_comment_model = Comments::createNewCommentObject($model->article_id, $model->user_id);

        return $this->renderPartial('comment_form', [
            'comment_model' => $new_comment_model,
            'expanded_comments_ids' => explode(',', $expanded_ids)
        ]);
    }

    public function actionDelete($id, $article_id, $expanded_ids)
    {
        $comment = Comments::findOne($id);
        if ($comment && $comment->canBeDeleted()) {
            $comment->delete();
        }

        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => explode(',', $expanded_ids),
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }

    public function actionEdit($id, $article_id, $expanded_ids)
    {
        $comment = Comments::findOne($id);
        if ($comment && $comment->canBeDeleted()) {
            return $this->renderPartial('comments_list', [
                'comments' => Comments::getArticleBaseComments($article_id),
                'expanded_comments_ids' => explode(',', $expanded_ids),
                'edited_comment_id' => $comment->id,
                'answer_comment_id' => null
            ]);
        }

        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => explode(',', $expanded_ids),
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }

    public function actionSave($id, $article_id, $expanded_ids)
    {
        $comment = Comments::findOne($id);
        if ($comment && $comment->canBeDeleted() && $comment->load(\Yii::$app->request->post())) {
            $comment->save();
        }

        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => explode(',', $expanded_ids),
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }

    public function actionAnswer($id, $article_id, $expanded_ids)
    {
        $comment = Comments::findOne($id);
        if ($comment) {
            return $this->renderPartial('comments_list', [
                'comments' => Comments::getArticleBaseComments($article_id),
                'expanded_comments_ids' => explode(',', $expanded_ids),
                'edited_comment_id' => null,
                'answer_comment_id' => $id
            ]);
        }

        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => explode(',', $expanded_ids),
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }

    public function actionReply($article_id, $expanded_ids)
    {
        $model = new Comments();
        if ($model->load(\Yii::$app->request->post())) {
            $model->save();
        }
        $expanded_ids = array_merge(explode(',', $expanded_ids), [$model->parent_id]);
        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => $expanded_ids,
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }

    public function actionExpand($id, $article_id, $expanded_ids)
    {
        $expanded_ids = array_merge(explode(',', $expanded_ids), [$id]);
        return $this->renderPartial('comments_list', [
            'comments' => Comments::getArticleBaseComments($article_id),
            'expanded_comments_ids' => $expanded_ids,
            'edited_comment_id' => null,
            'answer_comment_id' => null
        ]);
    }
}
